#include <iostream>
#include "list.h"
using namespace std;
template <class A>
List<A>::~List() {
	for (Node<A> *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}
template <class A>
void List<A>::pushToHead(A el)
{
	head = new Node<A>(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}
template <class A>
void List<A>::pushToTail(A el)
{
	//TO DO!
	tail = new Node<A>(el, 0, tail);
	if (head == 0)
	{
		head = tail;
	}
	else
	{
		tail->prev->next = tail;
	}
}
template <class A>
A List<A>::popHead()
{
	A el = head->data;
	Node<A> *tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
template <class A>
A List<A>::popTail()
{
	// TO DO!
	A el = tail->data;
	Node<A> *tmp = tail;
	if (tail == head)
	{
		tail = head = 0;
	}
	else
	{
		tail = tail->prev;
		tail->next = 0;
	}
	delete tmp;
	return el;
}
template <class A>
bool List<A>::search(A el)
{
	// TO DO! (Function to return True or False depending if a Aacter is in the list.
	for (Node<A> *tmp = head; tmp != 0; tmp = tmp->next) {
		if (tmp->data == el)
			return true;
	}
	return false;
}
template <class A>
void List<A>::print()
{
	for (Node<A> *tmp = head; tmp != 0; tmp = tmp->next) {
		cout << tmp->data;
	}
	cout << endl;
}