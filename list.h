#ifndef LIST
#define LIST

template <class A>
class Node {
public:
	A data;
	Node<A>*next, *prev;
	Node()
	{
		next = prev = 0;
	}
	Node(A el, Node<A> *n = 0, Node<A>*p = 0)
	{
		data = el; next = n; prev = p;
	}
};
template <class A>
class List {
public:
	List() { head = tail = 0; }
	int isEmpty() { return head == 0; }
	~List();
	void pushToHead(A el);
	void pushToTail(A el);
	A popHead();
	A popTail();
	bool search(A el);
	void print();
private:
	Node<A> *head, *tail;
};

#endif