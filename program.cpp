#include <iostream>
#include "list.h"
#include "list.cpp"
using namespace std;

void main()
{
	//Sample Code
	List<char> mylist;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');
	mylist.pushToTail('k');
	mylist.pushToTail('a');
	mylist.print();

	mylist.popHead();
	mylist.popTail();
	mylist.print();
	
	cout << "Please Enter the character" << endl;

	if (mylist.search(cin.get())) {

		cout << "IS IN LIST" << endl;
	}
	else 
	{
		cout << "ISN'T IN LIST" << endl;
	}

	system("pause");
	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?

}